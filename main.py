import time
import copy


def init_world(width, height):
    """Initialize the world with a given width and height."""
    grid = list()
    for i in range(height):
        grid.append(list())

    for lines in grid:
        for j in range(width):
            lines.append('.')
    return grid


def print_world(grid):
    """Print the world."""
    for lines in grid:
        for cell in lines:
            print(cell, end='')
        print()


def get_neighbours(grid, x, y):
    """Return the number of cell alive around a specific one."""
    neighbour = 0
    if grid[x-1][y] == '*':
        neighbour += 1
    if grid[x+1][y] == '*':
        neighbour += 1
    if grid[x][y-1] == '*':
        neighbour += 1
    if grid[x][y+1] == '*':
        neighbour += 1
    if grid[x+1][y+1] == '*':
        neighbour += 1
    if grid[x-1][y-1] == '*':
        neighbour += 1
    if grid[x+1][y-1] == '*':
        neighbour += 1
    if grid[x-1][y+1] == '*':
        neighbour += 1
    return neighbour


def go():
    """Main function that run the program."""
    world = init_world(20, 15)
    # a bar
    world[10][10] = '*'
    world[10][11] = '*'
    world[10][12] = '*'
    # a square
    world[5][7] = '*'
    world[6][7] = '*'
    world[5][8] = '*'
    world[6][8] = '*'

    while True:
        print_world(world)
        tmp_world = copy.deepcopy(world)
        w = 0
        while w < len(tmp_world) - 1:
            h = 0
            while h < len(tmp_world[w]) - 1:
                neighbour = get_neighbours(world, w, h)
                if world[w][h] == '*':  # if the cell is alive
                    if neighbour != 2 and neighbour != 3:
                        tmp_world[w][h] = '.'
                else:  # if the cell is dead
                    if neighbour == 3:
                        tmp_world[w][h] = '*'
                h += 1
            w += 1
        world = copy.deepcopy(tmp_world)
        print('----------------------------------------')
        time.sleep(1)
go()
